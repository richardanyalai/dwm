/* See LICENSE file for copyright and license details. */

#include <X11/XF86keysym.h>

/* appearance */
static const unsigned int borderpx       = 2;  /* border pixel of windows */
static const unsigned int snap           = 32; /* snap pixel */
static const unsigned int gappih         = 20; /* horiz inner gap between windows */
static const unsigned int gappiv         = 10; /* vert inner gap between windows */
static const unsigned int gappoh         = 10; /* horiz outer gap between windows and screen edge */
static const unsigned int gappov         = 10; /* vert outer gap between windows and screen edge */
static       int smartgaps               = 0;  /* 1 means no outer gap when there is only one window */
static const int swallowfloating         = 0;  /* 1 means swallow floating windows by default */
static const unsigned int systraypinning = 0;  /* 0: sloppy systray follows selected monitor, >0: pin systray to monitor X */
static const unsigned int systrayonleft  = 0;  /* 0: systray in the right corner, >0: systray on left of status text */
static const int systrayspacing          = 0;  /* systray spacing */
static const int systraypinningfailfirst = 1;  /* 1: if pinning fails, display systray on the first monitor, False: display systray on the last monitor*/
static const int showsystray             = 1;  /* 0 means no systray */
static const int showbar                 = 1;  /* 0 means no bar */
static const int topbar                  = 1;  /* 0 means bottom bar */
static const char *fonts[]               = { "Hack Nerd Font:size=10", "Font Awesome:size=10", "Noto Color Emoji:size=10" };
static const char dmenufont[]            = "Font Awesome:size=10";
static const char col_gray1[]            = "#191617";
static const char col_gray2[]            = "#444444";
static const char col_gray3[]            = "#bbbbbb";
static const char col_gray4[]            = "#ffffff";
static const char col_blue[]             = "#0d32b8";
static const char *colors[][3]           = {
	/*               fg         bg         border   */
	[SchemeNorm] = { col_gray3, col_gray1, col_gray2 },
	[SchemeSel]  = { col_gray4, col_blue,  col_blue  },
};

/* tagging */
static const char *tags[] = { "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	 /* class  | instance | title | tags mask | isfloating | isterminal | noswallow | monitor */
	 { "St",     NULL,      NULL,   0 << 0,     0,           1,           0,          -1 },
	 { "Gimp",   NULL,      NULL,   1 << 4,     0,           0,           0,          -1 },
	 { "Brave",  NULL,      NULL,   1 << 0,     0,           0,           0,          -1 },
	 { NULL,     NULL,    "QEMU",   0 << 0,     1,           0,           0,          -1 },
	 { NULL,     NULL,    "MI 9",   0 << 0,     1,           0,           0,          -1 }
};

/* layout(s) */
static const float mfact = 0.55; /* factor of master area size [0.05..0.95] */
static const int   nmaster = 1;    /* number of clients in master area */
static const int   resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static const int   lockfullscreen = 1;    /* 1 will force focus on the fullscreen window */

#define FORCE_VSPLIT 1
#include "vanitygaps.c"
#include "nextprevtag.c"

static const Layout layouts[] = {
	/* symbol  arrange function */
	{ "[]=", tile },    /* first entry is default */
	{ "><>", NULL },    /* no layout function means floating behavior */
	{ "[M]", monocle },
	{ "[@]", spiral },
	{ "[\\]",dwindle },
	{ "H[]", deck },
	{ "TTT", bstack },
	{ "===", bstackhoriz },
	{ "HHH", grid },
	{ "###", nrowgrid },
	{ "---", horizgrid },
	{ ":::", gaplessgrid },
	{ "|M|", centeredmaster },
	{ ">M>", centeredfloatingmaster },
	{ NULL,  NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ MODKEY,                       KEY,      view,           {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ MODKEY|ShiftMask,             KEY,      tag,            {.ui = 1 << TAG} }, \
	{ MODKEY|ControlMask|ShiftMask, KEY,      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define CMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }

/* commands */
static char dmenumon[2] = "0"; /* component of dmenucmd, manipulated in spawn() */
static const char *dmenucmd[] = { "dmenu_run", "-m", dmenumon, "-fn", dmenufont, "-nb", col_gray1, "-nf", col_gray3, "-sb", col_blue, "-sf", col_gray4, NULL };

static Key keys[] = {
	/* modifier           key                       function           argument */

	// function keys
	{      0,             XF86XK_AudioLowerVolume,  spawn,             CMD("$scripts/soundconf -") },
	{      0,             XF86XK_AudioRaiseVolume,  spawn,             CMD("$scripts/soundconf +") },
	{      0,             XF86XK_AudioMute,         spawn,             CMD("pactl -- set-sink-mute @DEFAULT_SINK@ toggle") },
	{      0,             XF86XK_AudioMicMute,      spawn,             CMD("pactl set-source-mute 1 toggle") },
	{      0,             XF86XK_AudioPlay,         spawn,             CMD("playerctl play-pause") },
	{      0,             XF86XK_AudioPause,        spawn,             CMD("playerctl play-pause") },
	{      0,             XF86XK_AudioStop,         spawn,             CMD("playerctl stop") },
	{      0,             XF86XK_AudioPrev,         spawn,             CMD("playerctl previous") },
	{      0,             XF86XK_AudioNext,         spawn,             CMD("playerctl next") },
	{      0,             XF86XK_MonBrightnessDown, spawn,             CMD("$scripts/brightness -") },
	{      0,             XF86XK_MonBrightnessUp,   spawn,             CMD("$scripts/brightness +") },
	{ MODKEY,             XF86XK_MonBrightnessDown, spawn,             CMD("$scripts/brightness - 10") },
	{ MODKEY,             XF86XK_MonBrightnessUp,   spawn,             CMD("$scripts/brightness + 10") },
	{ Mod1Mask,           XF86XK_MonBrightnessDown, spawn,             CMD("bulb down") },
	{ Mod1Mask,           XF86XK_MonBrightnessUp,   spawn,             CMD("bulb up") },
	{      0,             XK_F10,                   spawn,             CMD("$scripts/lockscreen") },
	{      0,             XF86XK_Calculator,        spawn,             CMD("$TERMINAL -e python") },
	{      0,             XK_Print,                 spawn,             CMD("flameshot gui") },

	// letter keys
	{ MODKEY,             XK_b,                     togglebar,         {0} },
	{ MODKEY,             XK_c,                     spawn,             CMD("brave --password-store=gnome") },
	{ MODKEY | ShiftMask, XK_c,                     cyclelayout,       {.i = +1 } },
	{ MODKEY,             XK_d,                     incnmaster,        {.i = -1 } },
	{ MODKEY,             XK_e,                     spawn,             CMD("$TERMINAL -e ranger") },
	{ MODKEY,             XK_f,                     setlayout,         {0} },
	{ MODKEY | ShiftMask, XK_f,                     togglefullscreen,  {.v = &layouts[1]} },
	{ MODKEY,             XK_h,                     setmfact,          {.f = -0.05} },
	{ MODKEY,             XK_i,                     incnmaster,        {.i = +1 } },
	{ MODKEY | ShiftMask, XK_i,                     spawn,             CMD("xcalib -invert -alter") },
	{ MODKEY,             XK_j,                     focusstack,        {.i = +1 } },
	{ MODKEY,             XK_k,                     focusstack,        {.i = -1 } },
	{ MODKEY,             XK_l,                     setmfact,          {.f = +0.05} },
	{ MODKEY,             XK_m,                     spawn,             CMD("telegram-desktop") },
	{ MODKEY | Mod1Mask,  XK_m,                     spawn,             CMD("$scripts/memes") },
	{ MODKEY | ShiftMask, XK_m,                     setlayout,         {.v = &layouts[2]} },
	{ MODKEY,             XK_o,                     spawn,             CMD("bulb tog") },
	{ MODKEY,             XK_q,                     killclient,        {0} },
	{ MODKEY,             XK_r,                     spawn,             {.v = dmenucmd } },
	{ MODKEY | ShiftMask, XK_r,                     quit,              {0} },
	{ MODKEY,             XK_s,                     togglesticky,      {0} },
	{ MODKEY,             XK_t,                     setlayout,         {0} },
	{ MODKEY | ShiftMask, XK_t,                     togglealwaysontop, {.v = &layouts[0]} },
	{ MODKEY | ShiftMask, XK_u,                     spawn,             CMD("$scripts/dmenuunicode") },
	{ MODKEY,             XK_v,                     spawn,             CMD("pkill mpv || mpv --demuxer-lavf-format=video4linux2 --demuxer-lavf-o-set=input_format=mjpeg av://v4l2:/dev/video0 --profile=low-latency --untimed") },
	{ MODKEY,             XK_w,                     spawn,             CMD("du -a $wallpapers/* | awk '{print $2}' | rofi -dmenu | xargs -r $scripts/wallpaper") },
	{ MODKEY,             XK_y,                     incrgaps,          {.i = +1 } },
	{ MODKEY | ShiftMask, XK_y,                     incrgaps,          {.i = -1 } },
	{ MODKEY,             XK_z,                     zoom,              {0} },

	// other keys
	{ MODKEY,             XK_Escape,                view_adjacent,     {.i = -1 } },
	{ MODKEY,             XK_Next,                  view_adjacent,     {.i = +1 } },
	{ MODKEY,             XK_Return,                spawn,             CMD("$TERMINAL") },
	{ MODKEY,             XK_Tab,                   view,              {0} },
	{ MODKEY,             XK_space,                 setlayout,         {0} },
	{ MODKEY | ShiftMask, XK_space,                 togglefloating,    {0} },
	{ MODKEY,             XK_comma,                 focusmon,          {.i = -1 } },
	{ MODKEY | ShiftMask, XK_comma,                 tagmon,            {.i = -1 } },
	{ MODKEY,             XK_period,                focusmon,          {.i = +1 } },
	{ MODKEY | ShiftMask, XK_period,                tagmon,            {.i = +1 } },
	
	// workspaces
	TAGKEYS(0x002b, 0) // 1 (+)
	TAGKEYS(0x01b5, 1) // 2 (ľ)
	TAGKEYS(0x01b9, 2) // 3 (š)
	TAGKEYS(0x01e8, 3) // 4 (č)
	TAGKEYS(0x01bb, 4) // 5 (ť)
	TAGKEYS(0x01be, 5) // 6 (ž)
	TAGKEYS(0x00fd, 6) // 7 (ý)
	TAGKEYS(0x00e1, 7) // 8 (á)
	TAGKEYS(0x00ed, 8) // 9 (í)
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static Button buttons[] = {
	/* click         event mask   button   function        argument */
	{ ClkLtSymbol,   0,           Button1, setlayout,      {0} },
	{ ClkLtSymbol,   0,           Button3, setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,   0,           Button2, zoom,           {0} },
	{ ClkStatusText, 0,           Button2, spawn,          CMD("$TERMINAL") },
	{ ClkClientWin,  MODKEY,      Button1, movemouse,      {0} },
	{ ClkClientWin,  MODKEY,      Button2, togglefloating, {0} },
	{ ClkClientWin,  MODKEY,      Button3, resizemouse,    {0} },
	{ ClkTagBar,     0,           Button1, view,           {0} },
	{ ClkTagBar,     0,           Button3, toggleview,     {0} },
	{ ClkTagBar,     MODKEY,      Button1, tag,            {0} },
	{ ClkTagBar,     MODKEY,      Button3, toggletag,      {0} },
};
